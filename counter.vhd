library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;

entity COUNTER is
  generic (
    NBITS: natural
  );
  port (
    RST_N: in  std_logic;
    CLK  : in  std_logic;
    CE   : in  std_logic;
    Q    : out std_logic_vector (NBITS - 1 downto 0)
  );
end entity COUNTER;
 
architecture BEHAVIORAL of COUNTER is 
  signal q_i: unsigned(Q'range);
begin

  process (RST_N, CLK)
  begin
    if RST_N = '0' then
      q_i <= (others => '0');
    elsif rising_edge(CLK) then
      if CE = '1' then
        q_i <= q_i + 1;      
      end if;
    end if;
  end process;
  
  Q <= std_logic_vector(q_i);

end architecture BEHAVIORAL;