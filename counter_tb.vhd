library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
 
entity COUNTER_TB is
end COUNTER_TB;
 
architecture BEHAVIORAL of COUNTER_TB is 

  constant CNTR_WIDTH: positive :=  4;                -- Counter bus width
  constant CLK_PERIOD: time     := 10 ns;             -- Clock period
  constant DELAY     : time     := 0.1 * CLK_PERIOD;  -- Output settling time

  -- Component Declaration for the Unit Under Test (UUT)
  component COUNTER
    generic (
      NBITS: natural
    );
    port (
      RST_N: in  std_logic;
      CLK  : in  std_logic;
      CE   : in  std_logic;
      Q    : out std_logic_vector (NBITS - 1 downto 0)
    );
  end component;

  --Inputs
  signal RST_N: std_logic;
  signal CLK  : std_logic;
  signal CE   : std_logic;

 	--Outputs
  signal Q    : std_logic_vector(CNTR_WIDTH - 1 downto 0);

begin
 
	-- Instantiate the Unit Under Test (UUT)
  uut: COUNTER
    generic map (
      NBITS => CNTR_WIDTH
    )
    port map (
      RST_N => RST_N,
      CLK   => CLK,
      CE    => CE,
      Q     => Q
    );

  -- Clock process definitions
  clk_process: process
  begin
    wait for 0.5 * CLK_PERIOD;
    CLK <= '0';
    wait for 0.5 * CLK_PERIOD;
    CLK <= '1';
  end process;

  -- Stimulus process
  stim_proc: process

    variable last_q: std_logic_vector(Q'range);

  begin
    CE <= '0' after 0.25 * CLK_PERIOD; -- Clock enable start disabled.

    -- Test reset

    RST_N <= '0' after 0.25 * CLK_PERIOD, '1' after 0.75 * CLK_PERIOD; -- Generate reset pulse

    wait until RST_N = '0';  -- Wait for reset pulse
    wait for DELAY;          -- Wait outputs to settle
    assert Q = std_logic_vector(to_unsigned(0, Q'length))  -- Check count = 0
      report "RST_N malfunction."
      severity failure;

    -- Test clock enable

    last_q := Q;
    wait until RST_N = '1';  -- Wait for reset deasserted
    wait until CLK = '1';    -- Wait active clock edge
    wait for DELAY;          -- Wait outputs to settle
    assert Q = last_q        -- Check count does not change
      report "CE malfunction."
      severity failure;

    -- Test counting

    CE <= '1';                -- Enable counting
    for i in 1 to 2**Q'length loop
      wait until CLK = '1';  -- Wait active clock edge
      wait for DELAY;        -- Wait outputs to settle
      assert to_integer(unsigned(Q)) = i mod 2**Q'length  -- Check count & roll-over
        report "Count malfunction."
        severity failure;
    end loop;

    wait for CLK_PERIOD;

    -- Force simulation to terminate
    assert false
      report "[SUCCESS]: simulation finished."
      severity failure;
  end process;

end;
